(function($){

	/* ---------------------------------------------- /*
	 * Preloader
	/* ---------------------------------------------- */

	$(window).load(function() {
		$('#status').fadeOut();
		$('#preloader').delay(300).fadeOut('slow');
	});

	$(document).ready(function() {

		/* ---------------------------------------------- /*
		 * Smooth scroll / Scroll To Top
		/* ---------------------------------------------- */

		$('a[href*=#]').bind("click", function(e){
           
			var anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $(anchor.attr('href')).offset().top
			}, 1000);
			e.preventDefault();
		});

		$(window).scroll(function() {
			if ($(this).scrollTop() > 100) {
				$('.scroll-up').fadeIn();
			} else {
				$('.scroll-up').fadeOut();
			}
		});

		/* ---------------------------------------------- /*
		 * Navbar
		/* ---------------------------------------------- */

		$('.header').sticky({
			topSpacing: 0
		});

		$('body').scrollspy({
			target: '.navbar-custom',
			offset: 70
		})

        
        /* ---------------------------------------------- /*
		 * Skills
        /* ---------------------------------------------- */    
        //var color = $('#home').css('backgroundColor');

        $('.skills').waypoint(function(){
            $('.chart').each(function(){
            $(this).easyPieChart({
                    size:140,
                    animate: 2000,
                    lineCap:'butt',
                    scaleColor: false,
                    barColor: '#FF5252',
                    trackColor: 'transparent',
                    lineWidth: 10
                });
            });
        },{offset:'80%'});
        
        
        /* ---------------------------------------------- /*
		 * Quote Rotator
		/* ---------------------------------------------- */
       
			$( function() {
				/*
				- how to call the plugin:
				$( selector ).cbpQTRotator( [options] );
				- options:
				{
					// default transition speed (ms)
					speed : 700,
					// default transition easing
					easing : 'ease',
					// rotator interval (ms)
					interval : 8000
				}
				- destroy:
				$( selector ).cbpQTRotator( 'destroy' );
				*/

				$( '#cbp-qtrotator' ).cbpQTRotator();

			} );
		
        
		/* ---------------------------------------------- /*
		 * Home BG
		/* ---------------------------------------------- */

		$(".screen-height").height($(window).height());

		$(window).resize(function(){
			$(".screen-height").height($(window).height());
		});

		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
			$('#home').css({'background-attachment': 'scroll'});
		} else {
			$('#home').parallax('50%', 0.1);
		}


		/* ---------------------------------------------- /*
		 * WOW Animation When You Scroll
		/* ---------------------------------------------- */

		wow = new WOW({
			mobile: false
		});
		wow.init();


		/* ---------------------------------------------- /*
		 * E-mail validation
		/* ---------------------------------------------- */

		function isValidEmailAddress(emailAddress) {
			var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
			return pattern.test(emailAddress);
		};

		/* ---------------------------------------------- /*
		 * Contact form ajax
		/* ---------------------------------------------- */

		$('#contact-form').submit(function(e) {
			e.preventDefault();

			var c_name = $('#c_name').val();
			var c_email = $('#c_email').val();
			var c_message = $('#c_message ').val();
			var response = $('#contact-form .ajax-response');
			var fd = new FormData();
		    fd.append("email", $("#c_email").val());
		    fd.append("name", $("#c_name").val());
		    fd.append("message", $("#c_message").val());

			if (( c_name== '' || c_email == '' || c_message == '') || (!isValidEmailAddress(c_email) )) {
				response.fadeIn(500);
				response.html('<i class="fa fa-warning"></i> Please fix the errors and try again.');
			}
			
			else {				
			    $('#contact-form .ajax-hidden').fadeOut(500);
			    $.ajax({
		            type: "POST",
		            url: '/',
		            data: fd,
		            complete: function(){
		                alert("Sent!");
		            },
		            error: function(){
		                alert("Something went wrong!");
		            }
        		});
			    response.html("Message Sent. I will contact you asap. Thanks.").fadeIn(500);
			}
        	return false;
		});

	});

})(jQuery);

	var keyAllowed = {};

	$(document).on('keydown', function(e) {
	  if (keyAllowed[e.which] === false) return;
	  keyAllowed[e.which] = false;
	  if (e.which >= 65 && e.which <= 71) {
	    var k = String.fromCharCode(e.which).toLowerCase();
	    $("#" + k).addClass("active");
	    document.getElementById(k + 'Audio').play();
	    document.getElementById(k + 'Audio').currentTime = 0;
	    document.getElementById(k + 'Audio').volume = 0.1;
	  }
	}).keyup(function(e) {
	  if (e.which >= 65 && e.which <= 71) {
	    keyAllowed[e.which] = true;
	    var k = String.fromCharCode(e.which).toLowerCase();
	    $("#" + k).removeClass("active");
	  }
	});
	$(".box").bind('mousedown', function(e) {
	  $(this).addClass("active");
	  $(this).addClass("click");
	  var k = $(this).prop('id');
	  document.getElementById(k + 'Audio').play();
	  document.getElementById(k + 'Audio').currentTime = 0;
	  document.getElementById(k + 'Audio').volume = 0.1;
	});
	$(".box").bind('mouseleave mouseup', function() {
	  if ($(this).hasClass("click")) {
	    $(this).removeClass("active");
	    $(this).removeClass("click");
	  }
	});

	var aNote= document.getElementById('aAudio'),
	    bNote= document.getElementById('bAudio'),
	    cNote= document.getElementById('cAudio'),
	    dNote= document.getElementById('dAudio'),
	    eNote= document.getElementById('eAudio'),
	    fNote= document.getElementById('fAudio'),
	    gNote= document.getElementById('gAudio');

	$('#a').mousedown(function(){
	    aNote.play();
	    aNote.currentTime = 0;
	  	aNote.volume=0.1;
	  
	});

	$('#b').mousedown(function(){
	    bNote.play();
	    bNote.currentTime = 0;
	  	bNote.volume=0.1;
	});

	$('#c').mousedown(function(){
	    cNote.play();
	    cNote.currentTime = 0;
	  	cNote.volume=0.1;
	});

	$('#d').mousedown(function(){
	    dNote.play();
	    dNote.currentTime = 0;
	  	dNote.volume=0.1;
	});

	$('#e').mousedown(function(){
	    eNote.play();
	    eNote.currentTime = 0;
	  	eNote.volume=0.1;
	});

	$('#f').mousedown(function(){
	    fNote.play();
	    fNote.currentTime = 0;
	  	fNote.volume=0.1;
	});

	$('#g').mousedown(function(){
	    gNote.play();
	    gNote.currentTime = 0;
	  	gNote.volume=0.1;
	});

	$('#g').keydown(function(){
	    gNote.play();
	    gNote.currentTime = 0;
	  	gNote.volume=0.1;
	});
